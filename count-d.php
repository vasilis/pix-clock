<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Show the timestamps and if they’re missing</title>
	<style>
		html {
			scroll-behavior: smooth;
		}

		body {
			display: grid;
			grid-template-columns: 1fr;
			gap: 1em;
			counter-reset: hoi;
		}
		
		section {
			grid-column-start: 1;
			mix-blend-mode: color;
			width: min-content;
		}
		section:nth-of-type(1),
		section:nth-of-type(13) { grid-row-start: 1;}
		section:nth-of-type(2),
		section:nth-of-type(14) { grid-row-start: 2;}
		section:nth-of-type(3),
		section:nth-of-type(15) { grid-row-start: 3;}
		section:nth-of-type(4),
		section:nth-of-type(16) { grid-row-start: 4;}
		section:nth-of-type(5),
		section:nth-of-type(17) { grid-row-start: 5;}
		section:nth-of-type(6),
		section:nth-of-type(18) { grid-row-start: 6;}
		section:nth-of-type(7),
		section:nth-of-type(19) { grid-row-start: 7;}
		section:nth-of-type(8),
		section:nth-of-type(20) { grid-row-start: 8;}
		section:nth-of-type(9),
		section:nth-of-type(21) { grid-row-start: 9;}
		section:nth-of-type(10),
		section:nth-of-type(22) { grid-row-start: 10;}
		section:nth-of-type(11),
		section:nth-of-type(23) { grid-row-start: 11;}
		section:nth-of-type(12),
		section:nth-of-type(24) { grid-row-start: 12;}
		
		@media (min-width: 44em) {
			body {
				grid-template-columns: min-content min-content;
			}
			section:nth-of-type(even) {
				grid-column-start: 2;
			}
			section:nth-of-type(2),
			section:nth-of-type(14) { grid-row-start: 1;}
			section:nth-of-type(3),
			section:nth-of-type(15),
			section:nth-of-type(4),
			section:nth-of-type(16) { grid-row-start: 2;}
			section:nth-of-type(5),
			section:nth-of-type(17),
			section:nth-of-type(6),
			section:nth-of-type(18) { grid-row-start: 3;}
			section:nth-of-type(7),
			section:nth-of-type(19),
			section:nth-of-type(8),
			section:nth-of-type(20) { grid-row-start: 4;}
			section:nth-of-type(9),
			section:nth-of-type(21),
			section:nth-of-type(10),
			section:nth-of-type(22) { grid-row-start: 5;}
			section:nth-of-type(11),
			section:nth-of-type(23),
			section:nth-of-type(12),
			section:nth-of-type(24) { grid-row-start: 6;}
		}
		@media (min-width: 66em) {
			
			section:nth-of-type(1n) {
				grid-column-start: 1;
			}
			section:nth-of-type(3n + 2) {
				grid-column-start: 2;
			}
			section:nth-of-type(3n) {
				grid-column-start: 3;
			}
			section:nth-of-type(2),
			section:nth-of-type(14),
			section:nth-of-type(3),
			section:nth-of-type(15) { grid-row-start: 1;}
			section:nth-of-type(4),
			section:nth-of-type(16),
			section:nth-of-type(5),
			section:nth-of-type(17),
			section:nth-of-type(6),
			section:nth-of-type(18) { grid-row-start: 2;}
			section:nth-of-type(7),
			section:nth-of-type(19),
			section:nth-of-type(8),
			section:nth-of-type(20),
			section:nth-of-type(9),
			section:nth-of-type(21) { grid-row-start: 3;}
			section:nth-of-type(10),
			section:nth-of-type(22),
			section:nth-of-type(11),
			section:nth-of-type(23),
			section:nth-of-type(12),
			section:nth-of-type(24) { grid-row-start: 4;}
		}
		@media (min-width: 88em) {
			body {
				grid-template-columns: min-content min-content min-content min-content;
			}
			section:nth-of-type(1n) {
				grid-column-start: 1;
			}
			section:nth-of-type(4n + 2) {
				grid-column-start: 2;
			}
			section:nth-of-type(4n + 3) {
				grid-column-start: 3;
			}
			section:nth-of-type(4n) {
				grid-column-start: 4;
			}
			section:nth-of-type(1n) { grid-row-start: 1;}
			section:nth-of-type(1n + 5) { grid-row-start: 2;}
			section:nth-of-type(1n + 9) { grid-row-start: 3;}
			section:nth-of-type(1n + 13) { grid-row-start: 1;}
			section:nth-of-type(1n + 17) { grid-row-start: 2;}
			section:nth-of-type(1n + 21) { grid-row-start: 3;}
			
		}
		
		section:nth-of-type(1n + 13) h2,
		section:nth-of-type(1n + 13) ul:after {
			text-align: right;
			grid-column-end: -1;
		}
		h2 {
			margin: 0;
			z-index: 3;
			color: hsl(106, 56%, 35%);
		}
		section:nth-of-type(1n + 13) h2 {
			color: hsl(350, 78%, 66%);
		}
		ul {
			display: grid;
			grid-template-columns: repeat(10, 2em);
			list-style: none;
			text-align: right;
			margin: 0;
			padding: 0;
			counter-reset: ul;
			
		}
		ul:after {
			content: counter(ul);
			text-align: left;
			display: block;
		}
		[data-count] {
			background: limegreen;
			padding: .2em;
		}
		[data-count="0"] {
			background: crimson;
			color: white;
			counter-increment: hoi ul;
		}
		body:after {
			content: counter(hoi);
		}
	</style>
</head>


<body>

<?php

$singleFiles = glob('clock-pix/*d_1x.jpg');
$doubleFiles = glob('clock-pix/*d.jpg');
$doubleFiles = array_merge($singleFiles,$doubleFiles);
$count = ((count($doubleFiles) - count($singleFiles)) * 2 + count($singleFiles)) ;

$i = 0;
$j = 0;
$files = [];
while ($i < count($doubleFiles)) {
	$single = explode('1x', $doubleFiles[$i]);
	if ( count($single) == 1 ) {
		$tt = explode('clock-pix/', $doubleFiles[$i]);
		$th = explode('_', $tt[1]);
		$thh = ($th[0] * 1) + 12;
		
		$files[$j] = 'clock-pix/'. $thh;
		$k = 1;
		while($k < count($th)) {
			$files[$j] .=  '_'. $th[$k];
			$k++;
		} 
		$j++;
	}
	if($doubleFiles[$i]) {
		$files[$j] = $doubleFiles[$i];
		$j++;
	}
	$i++;
}
rsort($files);
//var_dump($files);

$i = 0;
while ($i < 24){
	echo "<section id='t$i'><h2>$i</h2>\n<ul>";
	$h = $i;
	if ($i < 10) $h = "0" . $i;
	$j = 0;
	while ($j < 60) {
		$m = $j;
		if ($j < 10) $m = "0" . $j;
		$matches  = preg_grep('/\/'.$h.'_'.$m.'_/', $files);
		//var_dump($matches);
		echo "<li title='" . count($matches) . "' data-count='" . count($matches) . "'>$j</li>";
		$j++;
	}
	echo "</ul></section>";
	$i++;
}

//include('generate-cache.php');
?>
<script>
var d = new Date(); 
var h = d.getHours();
window.location.hash = '#t' + h;

</script>