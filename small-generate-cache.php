<?php


// Loop through all files and put them in an array

$divs = '';

$allFiles = glob('clock-pix/*.jpg'); // All files, including the single files!

$i = 0;
$j = 0;
$files = []; // We need a new array, because the amount of timestamps is not the same as the amount of files

while ($i < count($allFiles)) {
	// There are files that only represent a single timestamp, like 00_01_19_1x.jpg
	$single = explode('1x', $allFiles[$i]);
	// If it’s not a single timestamp, it can be used twice a day
	if ( count($single) == 1 ) {
		// First get the hour
		$tt = explode('clock-pix/', $allFiles[$i]);
		$th = explode('_', $tt[1]);
		// Add twelve to the hour
		$thh = ($th[0] * 1) + 12;
		
		// Add it all together and add it to the array
		$files[$j] = 'clock-pix/'. $thh;
		$k = 1;
		while($k < count($th)) {
			$files[$j] .=  '_'. $th[$k];
			$k++;
		} 
		$j++;
	}
	// Add the file to the file array.
	if($allFiles[$i]) {
		$files[$j] = $allFiles[$i];
		$j++;
	}
	$i++;
}


// Sort the files
sort($files);


$i = 0;
$hm = ''; // a check, should be empty at first
while ($i < count($files)) {
	// Get the time stamps
	$tt = explode('/', $files[$i]);
	$tt = explode('_', $tt[1]);
	
	// This is the time, which will be added to a data-time attribute to the containing div …
	$thm = "$tt[0]:$tt[1]";
	
	$div = '';
	$ffile = $files[$i];
	
	// Here we check if the file exists. If it doesn't, it means that the file is twelve hours ago.
	if (!file_exists("$files[$i]")) {
		// So right here, something like 19_37_01.jpg turns into 07_37_01.jpg
		$hour = $tt[0] * 1 - 12;
		if($hour < 10) {$hour = '0' . $hour;}
		$ffile = str_replace('/'.$tt[0].'_', '/'.$hour.'_', $files[$i]);
	}
	
	// If the timestamp is different that the previous one we need a new div
	if ($thm !== $hm) {
		$div = "<div data-time='$thm' aria-hidden='true'";
		$hm = $thm;
	}
	// Set a default alt text
	$alt = "$tt[0]:$tt[1]";
	if (file_exists("$ffile.txt")) {
		// If there’s a file with a better alt text, use that
		$alt = htmlspecialchars(file_get_contents("$ffile.txt"));
		
	}
	
	// The srcset part
	$src = $ffile; // Set the source of the image to the original file
	$srcset = '';
	$src = str_replace('clock-pix/', 'clock-pix/480/', $ffile); // 480 version
	$style = '';
	if(file_exists("../ipix-watch-clock/move/$ffile.txt")) {
		$style = file_get_contents("../ipix-watch-clock/move/$ffile.txt");
	}
	
	// Write the div to cache.
	$divs .= "$div id='img$i' data-src='$src' data-style='$style;'></div>\n";
	
	$i++;
}

file_put_contents('small.txt', $divs);

?>
ok.